--- Practica Programacion Declarativa, Janira Ortiz Hernandez
--- Metodo simplex para problemas de programacion lineal con restricciones de igualdad o desigualdad <=

--- Se importan los modulos para acceder a las funciones que se utilizaran durante el desarrollo del programa:
import Data.List
import Data.Maybe


--- Definimos los tipos Vector y Matriz (listas y listas de listas) que resultaran muy utiles a la hora de trabajar con estas estructuras:
type Matriz = [[Float]]
type Vector = [Float]


--- Funciones para la multiplicacion de vectores y matrices
mulVect :: Vector -> Vector -> Float
mulVect xs ys = sum (zipWith (*)  xs ys)

mulMatrices :: Matriz -> Matriz -> Matriz
mulMatrices xss yss = transpose [[mulVect (xss!!i) ((transpose yss)!!j) | i <- [0..(length xss - 1)]] | j <- [0..(length (transpose yss) - 1)]]

mulMatVec :: Matriz -> Vector -> Vector
mulMatVec xss xs = [ mulVect (xss!!i) xs | i <- [0..(length xss - 1)]]

mulVecMat :: Vector -> Matriz -> Vector
mulVecMat xs xss = [ mulVect xs ((transpose xss)!!i) | i <- [0..(length (transpose xss) - 1)]]


{-Para poder crear la funcion que permita calcular la inversa de una matriz se utilizaran las siguientes funciones auxiliares: 
  det: calculo del determinante de una matriz cuadrada de cualquier tamaño (haciendo uso de la definicion del determinante de tamaño 1,2 y 3
  eliminarFila: eliminacion de cualquier fila de la matriz dada para poder hacer el calculo de los determinantes de orden mayor que 3
  eliminarCol: analogamente se requiere de la eliminacion de columnas para el calculo de los determinantes de mayor orden:-}

eliminarFila :: Int -> Matriz -> Matriz	
eliminarFila i xss = [xss!!k | k <- filter (/=i) [0..((length xss) -1)]]

eliminarCol :: Int -> Matriz -> Matriz	
eliminarCol j xss = transpose (eliminarFila j (transpose xss))

det33 :: Matriz -> Float
det33 xss = ( xss!!0!!0 * xss!!1!!1 * xss!!2!!2 + xss!!1!!0 * xss!!2!!1 * xss!!0!!2 + xss!!1!!2 * xss!!0!!1 * xss!!2!!0 ) -
			( xss!!0!!2 * xss!!1!!1 * xss!!2!!0 + xss!!1!!2 * xss!!2!!1 * xss!!0!!0 + xss!!1!!0 * xss!!0!!1 * xss!!2!!2 )

det :: Matriz -> Float
det xss
    | length xss == 1 = xss!!0!!0
	| length xss == 2 = ( xss!!0!!0 * xss!!1!!1 ) - ( xss!!0!!1 * xss!!1!!0 )
	| length xss == 3 = det33 xss
	| length xss == 4 = sum [((-1)^i) * xss!!0!!i * (det33 (eliminarFila 0 (eliminarCol i xss))) | i <- [0..(length (xss!!0) -1)]]
	| otherwise = sum[((-1)^i )* xss!!0!!i * (det (eliminarFila 0 (eliminarCol i xss))) | i <- [0..(length (xss!!0) -1)]]
	
inversa :: Matriz -> Matriz
inversa xss = [map (*(1/(det xss))) ((transpose (adjunta xss))!!k) | k <- [0..((length xss) - 1)]]
  where adjunta xss = [[((-1)^(j+i))* det (eliminarFila i (eliminarCol j xss)) | j <- [0..(length (xss!!i) - 1)]] | i <- [0..(length (xss!!0) - 1)]]

  
--- Comenzamos con la definicion de las funciones propias del metodo simplex:


--- Se toma una base incial, indicando el intervalo i j de las variables basicas que la forman:
cogeBase :: Int -> Int -> Matriz -> Vector -> (Matriz,[Int])
cogeBase i j xss b = (transpose[(transpose xss)!!k | k <- [(i - 1)..(j - 1)]] , [(i - 1)..(j - 1)])


--- Se comprueba que la base tomada proporciona una solucion basica factible. En caso contrario se cambian las variables basicas hasta conseguir 
--- dicha solucion factible:
comprobarBase :: (Matriz,[Int]) ->  Matriz -> Vector -> (Matriz,[Int])
comprobarBase base xss xs
  | (esSolFactible (fst(base)) xs == True) = base
  | otherwise = comprobarBase otraBase xss xs
    where otraBase = cogeBase (snd(base)!!0 - 2)  (snd(base)!!((length (fst(base)!!0)) - 1)) xss xs

	
--- Una vez elegida la base inicial, se requiere una solucion basica factible, es decir, que el resultado de multiplicar por la izquierda 
--- al vector de terminos independientes la inversa de la base, se obtenga un vector con todas sus componentes mayores o iguales que cero:
esSolFactible :: Matriz -> Vector -> Bool
esSolFactible xss xs = and [(mulMatVec (inversa xss) xs)!!i >= 0 | i <- [0..((length xs) - 1)]]


--- Un problema de programacion lineal de minimizaion tiene solucion no acotada si para un valor positivo del vector zj-cj de la tabla de simplex su
--- correspondiente columna tiene todos sus numeros menores o iguales que cero:
esSolNoAcotada :: Vector -> Matriz -> Bool
esSolNoAcotada xs xss = or[if xs!!i > 0 then and[(transpose xss)!!i!!j <= 0 | j <- [0..(length ((transpose xss)!!i) - 1)]] 
										else False | i <- [0..(length xs - 1)]]

										
--- Se obtiene una tabla optima cuando todos los valores del vector zj-cj son menores o iguales a cero:		 
esTablaOptima :: Vector -> Bool
esTablaOptima xs = and[xs!!i <= 0 | i <- [0..((length xs) - 1)]]


--- Se calculan los costes de las variables basicas respecto de la funcion objetivo:
costesBase :: (Matriz,[Int]) -> Vector -> Vector
costesBase xss z = [z!!k | k <- snd(xss)]


--- Se calcula el vector zj-cj de la tabla de simplex, introducioendo como datos los costes de la base, la matriz de la tabla y el vector z de la funcion objetivo:
vectorZC :: Vector -> Matriz -> Vector -> Vector
vectorZC xs xss z = [(mulVecMat xs  xss)!!k - z!!k | k <- [0..((length z) - 1)]]


--- El valor de la funcion objetivo se calcula multiplicando los costes de las variables basicas por sus valores correspondientes a la tabla actual:
valorFunObj :: Vector -> Vector -> Float
valorFunObj xs ys = mulVect xs ys


--- Se calcula el pivote para la construccion de la siguiente tabla del metodo:
pivote :: Vector -> Matriz -> Vector -> (Float ,[Int])
pivote zs yss xs = (yss !!(fromJust(elemIndex (minimum pivs) pivs)) !!pos , [(fromJust(elemIndex (minimum pivs) pivs)),pos])
  where pos = fromJust(elemIndex (maximum zs) zs)
        colpiv = (transpose yss)!!pos
        pivs = [if colpiv!!k > 0 then (fst((zip xs colpiv)!!k) / snd((zip xs colpiv)!!k)) else (maximum colpiv) + 1 | k <- [0..((length xs) - 1)]]


--- Actualizacion de la base al realizar la entrada y salida de las variables correspondientes tras el calculo del pivote:
actualizarBase :: (Matriz,[Int]) -> (Float ,[Int]) -> Matriz -> (Matriz,[Int])
actualizarBase xs pivot xss = (transpose[if k == snd(pivot)!!0 then (transpose xss)!!(snd(pivot)!!1) else (transpose(fst(xs)))!!k | k <- [0..((length(fst(xs))) - 1)]] , 
					                    [if k == snd(pivot)!!0 then snd(pivot)!!1 else snd(xs)!!k | k <- [0..(length(snd(xs)) - 1)]])

										
--- Actualizacion de los datos centrales de la tabla de simplex respecto del pivote elegido:
actualizarTabla :: Matriz -> (Float ,[Int]) -> Matriz
actualizarTabla yss pivot = [if i == snd(pivot)!!0 then filaPivotNorm 
                             else  [(map (*(-yss!!i!!(snd(pivot)!!1))) filaPivotNorm)!!k + (yss!!i)!!k |k <- [0..((length (yss!!0)) -1)]]| i <- [0..((length ((transpose yss)!!0)) -1)]]
							 where filaPivot = yss!!(snd(pivot)!!0)
							       filaPivotNorm = map (/fst(pivot)) filaPivot


--- Actualizacion del vector de terminos independientes de la tabla respecto del pivote elegido:	
actualizarB :: Vector -> (Float ,[Int]) -> Matriz -> Vector
actualizarB ys pivot yss = [if k == snd(pivot)!!0 then ys!!(snd(pivot)!!0) / fst(pivot) 
				            else (-ys!!(snd(pivot)!!0) / fst(pivot)) * (yss!!k!!(snd(pivot)!!1)) + ys!!k | k <- [0..((length ys) - 1)]]

										   
--- Funcion que crea la matriz identidad de tamaño n para añadirla en el caso de proble de desigualdad (seran los datos correspondientes a las 
--- variables de holgura que hay que introducir:
identidad :: Num a => Int -> [[a]]
identidad n = [[if k == i then 1 else 0 | k <- [0..(n-1)]] | i <-[0..(n-1)]] 



--- Funciones auxiliares para leer y devolver la eleccion del tipo de problema a resolver:

leeEntero :: IO Int
leeEntero = do entero <- getLine
               return (read entero)
	   
leeOpcion12 :: IO Int
leeOpcion12 = do putStrLn "Dime si se trata de un problema de restrcciones de igualdad o desigualdad <=: "
                 putStrLn "1: Igualdad"
                 putStrLn "2: Desigualdad"
                 n <- leeEntero
                 if (n > 2) || (n < 1) then leeOpcion12
                  else return n

				  

--- Funcion auxiliar para convertir los datos datos de [Char] a Float:
rFloat :: String -> Float
rFloat = read

pasaDatos :: String -> [Float]
pasaDatos = map (rFloat).words

pasaDatos2 :: Int -> IO [[Float]]
pasaDatos2 n = do putStrLn "Dame coeficientes de cada restriccion: "
                  coefis <- getLine
                  
                  let coef = [pasaDatos coefis]
                  if n>0 then do let coefs = coef ++ pasaDatos2 (n-1)
                  return coefs
                  else return coef
                                  

{- El menu del programa comienza con la eleccion entre problema con restricciones de igualdad o desigualdad mediante la funcion leeOpcion12
y despues se pide al usuario la introducion de los datos inciales en formato de lista de numeros (lista de listas para la matriz de coeficientes 
de las restricciones).

Una vez leidos los datos se deriva a la opcion elegida al comienzo. En el caso de igualdad se calculan los datos principales para ejecutar
la funcion solucionSimplex que crea el resto de datos para la construccion de la primera tabla y su analisis.

El caso de desigualdad es analogo al anterior a excepcion de la introducion, en la funcion objetivo y en las restricciones, de los datos 
de las variables de holgura necesarias.-}

{-Dejo comentada la posibilidad de introducir los datos como string (mucho mas comodo), leerlos con getLine y despues convertirlos a listas
de numeros reales, puesto que, aunque he comprobado paso por paso que el programa deberia correr sin problema, se produce un error de indices. 
No he podido encontrar el origen de este error.

El error a la hora de dar el problema de desigualdad tampoco he conseguido detectarlo, aunque he comprobado funcion por funcion que todos
los calculos se deberian realizan correctamente. Introduciendo a mano las holguras en el caso de igualdad el problema se resuelve correctamente.-}
   
main = do opcion <- leeOpcion12
          putStrLn "Dime numero de restricciones del problema:"
          dimension <- getLine
          let n = read dimension
          xss <- pasaDatos2 n 
          putStrLn "Dame vector de coeficientes de la funcion objetivo:"
          zs <- getLine
          putStrLn "Dame vector b de terminos independientes:"
          bs <- getLine
          let z = pasaDatos zs
          let b = pasaDatos bs
          putStrLn "Ok, vamos"
          case opcion of
                   1 -> do let base1 = cogeBase ((length z) - ((length b) -1)) (length z) xss b
                           let base = comprobarBase base1 xss b                   
                           let xBarra = mulMatVec (inversa (fst(base))) b
                           let yss = mulMatrices (inversa (fst(base))) xss
                           solucionSimplex z base yss xBarra
					
                   2 -> do let zd = z ++ [0 | k <- [0..((length b) - 1)]]
                           let xssd = [xss!!k ++ (identidad (length b))!!k | k <- [0..((length b)-1)]]
                           let base = cogeBase ((length zd) - ((length b) -1)) (length zd) xssd b                 
                           let xBarra = mulMatVec (inversa (fst(base))) b
                           let yss = mulMatrices (inversa (fst(base))) xssd
                           solucionSimplex zd base yss xBarra
	   

solucionSimplex :: Vector -> (Matriz,[Int]) -> Matriz -> Vector -> IO ()
solucionSimplex z bs xss xs = do let cBase = costesBase bs z
                                 let zjMenoscj = vectorZC cBase xss z
                                 let zBarra = valorFunObj cBase xs
                                 if esSolNoAcotada zjMenoscj xss then noAcotada
                                   else do if esTablaOptima zjMenoscj then optima z bs xs zBarra
                                             else do let zs = z
                                                     let pivot = pivote zjMenoscj xss xs 
                                                     let yss = actualizarTabla xss pivot
                                                     let base = actualizarBase bs pivot xss
                                                     let xBarra = actualizarB xs pivot xss
                                                     solucionSimplex zs base yss xBarra

--- Caso de solucion no acotada del problema de programacion lineal.
noAcotada :: IO ()
noAcotada = do putStr "Solucion no acotada"

--- Caso de existencia de solucion optima del problema. Se muestra el valor de la funcion objetivo y de las variables, incluyendo las no basicas.
optima :: Vector -> (Matriz,[Int]) -> Vector -> Float -> IO ()
optima z bs xs zBarra = do putStrLn "Solucion optima encontrada: "
                           let valores = (zipWith (++) (map (++"=") (map ("x"++)(map (show) [k | k <- [1..(length z)]]))) [if elem k (snd(bs)) then show (xs!!(fromJust(elemIndex k (snd(bs))))) else "0" | k <- [0..((length z) - 1)]])
                           putStrLn ("Valor de la funcion objetivo: " ++ (show (zBarra)))
                           putStrLn ("Valores optimos: " ++ (show (unwords valores)))

