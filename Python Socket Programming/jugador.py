from paho.mqtt.client import Client
import pickle
from tkinter import Tk, Label, Button, Entry, StringVar, font, Scrollbar
import time

window = Tk()
window.geometry('1000x700')
window.configure(bg = 'CadetBlue', pady=5)
window.title("Información del juego")
window.resizable(width=True, height=True)

l = Label(window, text="¡Regístrate para recibir la información del juego!", height=15, width=80, font=font.Font(family="Helvetica", size=12, weight='bold'), background="CadetBlue", relief="groove", bd=4)
l.pack(expand=1, fill="both", pady=10)

yscroll = Scrollbar(l)
yscroll.pack(side = 'right', fill = 'y')

l = Label(window, text="¡Bienvenido y suerte!", font=font.Font(family="Helvetica", size=13, weight='bold'), bg="CadetBlue", relief="groove", bd=4)
l.pack(fill="both", pady=10)

def salir():
    window.destroy()

salir = Button(window, text="Salir", command=salir, font=font.Font(weight='bold'))
salir.place(x=905, y=650)

msg_l = Label(window,text='Nombre: ', font=font.Font(weight='bold'), background="CadetBlue")
msg_l.pack(pady=10)
msg_t = StringVar()
msg = Entry(window, width=20, textvariable=msg_t)    
msg.pack()
message = []

def ejec_button():
    client.publish('registro', pickle.dumps(msg.get()))
    nombre = msg.get()
    message.append(nombre) 
    msg.destroy()
    msg_l.destroy()
    send.destroy()
    
send = Button(window, text="Registrarse", command=ejec_button)
send.pack()

msg2_l = Label(window,text='? o ! (preguntar o responder): ', font=font.Font(weight='bold'), background="CadetBlue")
msg2_l.pack(pady=10)
msg2_t = StringVar()
msg2 = Entry(window, width=20, textvariable=msg2_t)    
msg2.pack()

    
def ejec_button2():
    if msg2.get() == '?':
        msg3_l = Label(window,text='Dime pregunta:', font=font.Font(weight='bold'), background="CadetBlue")
        msg3_l.pack(pady=10)
        msg3_t = StringVar()
        msg3 = Entry(window, width=20, textvariable=msg3_t)    
        msg3.pack()
        
        def enviar3():
            message.insert(0,msg3.get())
            send3.destroy()
            
        send3 = Button(window, text="Enviar", command=enviar3)
        send3.pack()
            
        msg4_l = Label(window,text='Dime solución:', font=font.Font(weight='bold'), background="CadetBlue")
        msg4_l.pack(pady=10)
        msg4_t = StringVar()
        msg4 = Entry(window, width=20, textvariable=msg4_t)    
        msg4.pack()
            
        def enviar4():
            msg3.destroy()
            msg3_l.destroy()
            message.insert(1,msg4.get())
            client.publish('?', pickle.dumps(message))
            del message[0]
            del message[0]
            msg4.destroy()
            msg4_l.destroy()
            send4.destroy()
            
        send4 = Button(window, text="Enviar", command=enviar4)
        send4.pack()

        
    elif msg2.get() == '!':
        msg3_l = Label(window,text='Dime pregunta a responder:', font=font.Font(weight='bold'), background="CadetBlue")
        msg3_l.pack(pady=10)
        msg3_t = StringVar()
        msg3 = Entry(window, width=20, textvariable=msg3_t)    
        msg3.pack()
        
        def enviar3():
            message.insert(0,msg3.get())
            send3.destroy()
            
        send3 = Button(window, text="Enviar", command=enviar3)
        send3.pack()
            
        msg4_l = Label(window,text='Dime respuesta:', font=font.Font(weight='bold'), background="CadetBlue")
        msg4_l.pack(pady=10)
        msg4_t = StringVar()
        msg4 = Entry(window, width=20, textvariable=msg4_t)    
        msg4.pack()
            
        def enviar4():
            msg3.destroy()
            msg3_l.destroy()
            message.insert(1,msg4.get())
            client.publish('!', pickle.dumps(message))
            del message[0]
            del message[0]
            msg4.destroy()
            msg4_l.destroy()
            send4.destroy()
            
        send4 = Button(window, text="Enviar", command=enviar4)
        send4.pack()

send2 = Button(window, text="Enviar", command=ejec_button2)
send2.pack()
    
def on_message(client, userdata, msg):
    tipo = type(pickle.loads(msg.payload))
    if tipo == list:
        client.window.winfo_children()[0].config(text=pickle.loads(msg.payload))
        client.window.winfo_children()[0].pack()
    else:
        client.window.winfo_children()[1].config(text=pickle.loads(msg.payload))
        client.window.winfo_children()[1].pack()
        time.sleep(1)

client = Client()

client.on_message = on_message
client.window = window

client.connect("iot.eclipse.org")

client.subscribe('info')

client.loop_start() 
    
client.window.mainloop() 
