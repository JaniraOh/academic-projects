from paho.mqtt.client import Client
import pickle

def anadirRanking(userdata, jugador):
    t = False
    for i in range(0, len(userdata[2])):
        if userdata[2][i][0] == jugador:
            t = True
    if not t:
        userdata[2].append([jugador, 0])

def preguntar(client, userdata, message):
    i = len(userdata[0]) + 1 #Para introducir el número de la pregunta
    preg = []
    for j in range(0, len(userdata[0])):
        preg.append(userdata[0][j][1])
    if message[0] not in preg:
        userdata[0].append([i, message[0], message[2]])
        userdata[1].append([i, message[1]])
        client.publish('info', pickle.dumps('Nueva pregunta de' + ' ' + str(message[2]) + ' : ' + str(message[0])))
        client.publish('info', pickle.dumps(str(message[2]) + ': ' + 'sumas 1 punto por la pregunta'))
        for j in range(0, len(userdata[2])):
            if userdata[2][j][0] == message[2]:
                userdata[2][j][1] = userdata[2][j][1] + 1
    else:
        client.publish('info', pickle.dumps(str(message[2]) + ': ' + 'esa pregunta ya está en la lista'))
    

def responder(client, userdata, message):
    try:
        k = int(message[0]) #Si se selecciona la pregunta por su número
        if message[2] == userdata[0][k-1][2]:
            client.publish('info', pickle.dumps(str(message[2]) + ': ' + '¡no puedes contestar tus propias preguntas!'))
        elif k-1 not in range(0, len(userdata[0])):
            client.publish('info', pickle.dumps(str(message[2]) + ': ' + 'no existe ese número de pregunta'))
        else:
            client.publish('info', pickle.dumps(str(message[2]) + ' ' + 'responde a la pregunta'+ ' ' + str(message[1])))
            if message[1] == userdata[1][k-1][1]:
                client.publish('info', pickle.dumps(str(message[2]) + ': ' + '¡Exacto! Ganas 3 puntos.'))
                for i in range(0, len(userdata[2])): 
                    if userdata[2][i][0] == message[2]:
                        userdata[2][i][1] = userdata[2][i][1] + 3
                del userdata[0][k-1]
                del userdata[1][k-1]
                for i in range(k-1, len(userdata[0])):
                    userdata[0][i][0] = userdata[0][i][0] - 1
                    userdata[1][i][0] = userdata[1][i][0] - 1
            else:
                client.publish('info', pickle.dumps(str(message[2]) + ': ' + 'fallaste, restas 2 puntos.'))
                for i in range(0, len(userdata[2])):
                        if userdata[2][i][0] == message[2]:
                            userdata[2][i][1] = userdata[2][i][1] - 2
                            
    except:  #Si se selecciona la pregunta escribiendola de nuevo
        preg = []
        for i in range(0, len(userdata[0])):
            preg.append(userdata[0][i][1])
        if message[0] not in preg:
            client.publish('info', pickle.dumps(str(message[2]) + ': ' + 'la pregunta que quieres contestar no existe'))
        for j in range(0, len(userdata[0])):
            if message[0] == preg[j]:
                if message[2] == userdata[0][j][2]:
                    client.publish('info', pickle.dumps(str(message[2]) + ': ' + 'no puedes contestar tus propias preguntas!'))
                else:
                    client.publish('info', pickle.dumps(str(message[2]) + ' ' + 'responde a la pregunta'+ ' ' + str(message[0])))
                    if message[1] == userdata[1][j][1]:
                        client.publish('info', pickle.dumps(str(message[2]) + ': ' + '¡exacto! Ganas 3 puntos.'))
                        for i in range(0, len(userdata[2])): 
                            if userdata[2][i][0] == message[2]:
                                userdata[2][i][1] = userdata[2][i][1] + 3
                        del userdata[0][j]
                        del userdata[1][j]
                        for i in range(j, len(userdata[0])):
                            userdata[0][i][0] = userdata[0][i][0] - 1
                            userdata[1][i][0] = userdata[1][i][0] - 1
                    else:
                        client.publish('info', pickle.dumps(str(message[2]) + ': ' + 'fallaste, restas 2 puntos.'))
                        for i in range(0, len(userdata[2])):
                            if userdata[2][i][0] == message[2]:
                                userdata[2][i][1] = userdata[2][i][1] - 2
        

def on_message(client, userdata, msg):
    try:
        message = pickle.loads(msg.payload)
        if msg.topic == 'registro':
            anadirRanking(userdata, message)
            preguntas = '\n'.join(list(map(str, userdata[0])))
            ranking = '\n'.join(list(map(str, userdata[2])))
            client.publish('info', pickle.dumps(['Preguntas disponibles:'+ '\n' + preguntas + '\n\n' + 'Ranking de jugadores'+ ':\n' + ranking]))
            print('Preguntas disponibles:'+ '\n' + preguntas + '\n' + 'Ranking de jugadores'+ ':\n' + ranking + '\n')
    
        if msg.topic == '?':
            preguntar(client, userdata, message)
            preguntas = '\n'.join(list(map(str, userdata[0])))
            ranking = '\n'.join(list(map(str, userdata[2])))
            client.publish('info', pickle.dumps(['Preguntas disponibles:'+ '\n' + preguntas + '\n\n' + 'Ranking de jugadores'+ ':\n' + ranking]))
            print('Preguntas disponibles:'+ '\n' + preguntas + '\n' + 'Ranking de jugadores'+ ':\n' + ranking + '\n')

        if msg.topic == '!':
            responder(client, userdata, message)
            preguntas = '\n'.join(list(map(str, userdata[0])))
            ranking = '\n'.join(list(map(str, userdata[2])))
            client.publish('info', pickle.dumps(['Preguntas disponibles:'+ '\n' + preguntas + '\n\n' + 'Ranking de jugadores'+ ':\n' + ranking]))
            print('Preguntas disponibles:'+ '\n' + preguntas + '\n' + 'Ranking de jugadores'+ ':\n' + ranking + '\n')
    except EOFError:
        print('Error. Mensaje no recibido.')
    except IOError:
        print('Error. Mensaje no enviado')
    except TypeError:
        print('Tipo de datos enviados inválido. ¡Vuelve a conectar tras modificarlo!')
    except pickle.UnpicklingError:
        print('Tipo de datos enviados inválido. ¡Vuelve a conectar tras modificarlo!')

LP = [] 
LR = []
RK = []
Datos = [LP, LR, RK]
    
client = Client(userdata=Datos)
client.on_message = on_message
client.connect("iot.eclipse.org")

client.subscribe('registro')
client.subscribe('?')
client.subscribe('!')            
    
client.loop_forever()
