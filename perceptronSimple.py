from __future__ import division, print_function

import numpy as np

class Perceptron2D(object):
    def __init__(self, dim):
        # We define the dimension because x and t are data that we request
        self.W = np.zeros(dim)
        self.b = np.zeros(1)

    def get_nn_value(self, x):
        # To get get_grad you need to go through the activation of a sigmoid
        y_data = np.dot(x, W) + b
        return 1 / (1 + np.exp(-y_data))

    def get_grad(self, x, t):
        # Calculate the gradient from x and t
        N = x.shape[0]
        y = self.get_nn_value(x)

        delta = y - t
        grad_W = np.sum(delta[:, np.newaxis] * x, axis=0) / N
        grad_b = np.sum(delta, axis=0) / N

        return (grad_W, grad_b)

    def train(self, x, t,
              epochs=20,
              batch_size=5,
              epsilon=0.1):
        # Perform the training of the neuron and returns us W and b
        L = range(10)
        np.random.shuffle(L)

        for i in range(x.shape[0] // batch_size):

            x_data2 = x[L[i: i + batch_size]]
            t_data2 = t[L[i: i + batch_size]]

            for _ in range(epochs):
                self.W = self.W - epsilon * self.get_grad(x_data2, t_data2)[0]
                self.b = self.b - epsilon * self.get_grad(x_data2, t_data2)[1]

        print(self.W, self.b)

    def classify(self, x, t):
        # Classify the neuron to know who is on one side(0) or another(1)
        tags = []
        i = 0
        L1 = x.dot(self.W)
        j = self.b.item(0)

        for i in L1:
            print(i + j, i + j >= 0)
            if i + j < 0:
                tags.append(0)

            else:
                tags.append(1)

        print(np.asarray(tags))
        return np.asarray(tags)

if __name__ == '__main__':

    N_black = 20
    N_red = 20
    x_data_black = np.random.randn(N_black, 2) + np.array([-10, 0])
    x_data_red = np.random.randn(N_red, 2) + np.array([10, 0])
    x_data = np.vstack((x_data_black, x_data_red))
    t_data = np.asarray([0]*N_black + [1]*N_red)
    W = np.random.randn(x_data.shape[1])
    b = np.random.randn(1)
    perceptron = Perceptron2D(2)
    perceptron.train(x_data, t_data, epochs=20, batch_size=5)
    perceptron.classify(x_data, t_data)

